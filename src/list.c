#include "list.h"
#include <stdlib.h>
#include <stdio.h>

list_t list_empty(){
list_t res=NULL;
return res;
}

int list_is_empty(list_t l){
  if(l==NULL){
    return 1;
  }
  return 0;
}

list_t list_push(list_t l, void* x){
  list_t c=malloc(sizeof(list_t));


  c->next=l;
  c->val=x;

  if (l!=NULL){
  c->id=l->id+1;
}
  else{
    c->id=1;

}
  return c;
}

list_t list_tail(list_t l){
  if(l!=NULL){
    return l->next;
  }
return NULL;
}

void* list_pop(list_t* l){
  if(l!=NULL){
    list_t tmp=(*l);
    (*l)=(*l)->next;
    void* res= tmp->val;
    free(tmp);
    return res;

  }
  return NULL;

}

void* list_top(list_t l){
  if(l!=NULL){

  return l->val;
}
return NULL;
}

void list_destroy(list_t l, void (*free_void)(void*)){
  while(l!=NULL){
    list_t tmp=l;
    free_void(tmp->val);
    l=l->next;
    free(tmp);

  }
  free(l);
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){
  if (l!=NULL){
    while (l->id!=1) {
      if (eq(l->val,x)==0){
        return l->val;
      }
      l=l->next;
    }
return NULL;


  }
}

unsigned long int list_len(list_t l){
  if(l!=NULL){
    return l->id;
  }
  return 0;
}
